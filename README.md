# SpaceJam
Control drones from the comfort of your phone

# Background
This is a solution for the coding challenge offered at http://challenge2.airtime.com:10001, the general idea is to start things off and allocate however many drone objects that you get back from the API.

Then you must do what you can to explore the 'labyrinth' in parallel while also making sure you don't exhaust the drones as they can only run 5 commands at a given time.

Finally, when you are finished your exploration aspect of the mission, you must correctly join together the found writings from the individual rooms that you have located throughout the labyrinth and post to the `/report` endpoint.

# Issues
* Seems like the documentation on the challenge page is somewhat out of date as `connectedRooms` no longer exists and is simply `connections` within the provided API.
* `409` no longer seems like the correct status code for the drone busy error, `400` is currently returned instead.

```
HTTP/1.1 400 Bad Request
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 62
ETag: W/"3e-WRJMTt16o4Puafe0FqJhaQ"
Date: Wed, 27 Jul 2016 02:55:54 GMT
Connection: keep-alive

{"error":"Drone id 3637f8d28597476f79c132addbd216c2 is busy."}
```

# Usage
* Open up `SpaceJam.xcworkspace`
* Build & Run

Optionally if you need to re-run `pod install` please use the provided `Gemfile` by typing `bundle exec pod install` so that the locked versions of the required gems specified in the `Gemfile` will be used.

> *NOTE:* You will need to have the Bundler gem installed prior to running `bundle exec pod install`, you can do this by running `gem install bundler`
