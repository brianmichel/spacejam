//
//  AirtimeChallengeAPI.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/24/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation
import Alamofire

typealias AirtimeChallengeAPICallback = (Response<AnyObject, NSError>) -> Void

/// API Wrapper for the Airtime Challenge API
final class AirtimeChallengeAPI {
    let manager: Manager
    let commander: String
    let baseURLString = "http://challenge2.airtime.com:10001"

    /**
     Initialize a new Airtime challenge API wrapper with the commander
     currently piloting the drones.

     - parameter commander: The email address of the commander piloting the drones.

     - returns: A newly initialized API wrapper ready to make API calls.
     */
    init(commander: String) {
        self.commander = commander

        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = [
            "x-commander-email": self.commander
        ]

        manager = Alamofire.Manager(configuration: configuration)
    }

    /**
     Submit an order to a drone for execution.

     - parameter droneID:  The identifier of the drone to submit the command to.
     - parameter commands: The list of commands to ask the drone to execute.
     - parameter callback: A callback with the result of the API request.
     */
    func order(drone droneID: String, commands: [DroneCommand], callback: AirtimeChallengeAPICallback) {
        var commandsToIssue = [String: [String: String]]()

        commands.map({$0.serialize()}).forEach { (serialized) in
            commandsToIssue[serialized.0] = serialized.1
        }

        manager.request(.POST, baseURLString + "/drone/\(droneID)/commands", parameters: commandsToIssue, encoding: .JSON)
            .validate().responseJSON { (response) in
                callback(response)
        }
    }

    /**
     Submit the start call to the API to gain start point information.

     - parameter callback: A callback with the result of the API request
     describing the initial room identifier and the drone ids.
     */
    func start(callback: AirtimeChallengeAPICallback) {
        manager.request(.GET, baseURLString + "/start")
            .validate()
            .responseJSON { (response) in
                callback(response)
        }
    }

    /**
     Submit a request to the API with a potential solution to the labyrinth.

     - parameter message:  The potential solution to the labyrinth challenge.
     - parameter callback: A callback with the result of the API request
     describing whether or not the submitted message is the correct answer to 
     the labyrinth challenge.
     */
    func report(message: String, callback: AirtimeChallengeAPICallback) {
        manager.request(.POST, baseURLString + "/report", parameters: ["message": message], encoding: .JSON)
            .validate()
            .responseJSON { (response) in
                callback(response)
        }
    }
}
