//
//  DroneCrawlerStrategy.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/24/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation
import SwiftyBeaver

/// A class used to encapsulate the crawling strategy for all drones.
final class DroneCrawlerStrategy: DroneResponseReceiver {
    private struct Constants {
        static let DroneCommandCapacity: UInt = 5
    }

    private var droneCommandsQueue = [DroneCommand]()
    private var writings: [(String, Int)] = []
    private var drones: [Drone]? = []

    private let API: AirtimeChallengeAPI
    private let solver: LabyrinthSolver

    private var exploredRoomCache: [String: Bool] = [:]

    /**
     Create a new DroneCrawlerStrategy with the desired drone ids, initial room id,
     and API.

     - parameter initialRoomId: The initial room id as provided by the start endpoint.
     - parameter droneIds:      The identifiers of the drones to use when requesting more information.
     - parameter API:           The API client instance to use for subsequent API calls.

     - returns: A newly initializes strategy object that will immediately start crawling the API.
     */
    init(initialRoomId: String, droneIds: [String], API: AirtimeChallengeAPI) {
        self.API = API
        
        self.solver = LabyrinthSolver(API: self.API, callback: { (status, solution) in
            switch status {
            case .Failed:
                Log.info("Failed to solve the labyrinth!")
            case .Solved:
                Log.info("Successfully solved the labyrinth!")
            }
        })

        self.drones = droneIds.map({ (identifier) -> Drone in
            Drone(identifier: identifier,
                maximumCommands: Constants.DroneCommandCapacity,
                responseReceiver: self,
                droneAPI: self.API
            )
        })

        did(findRooms: [initialRoomId])
    }

    private func crawl() {
        if let drones = drones {
            let availableDrones = drones.filter({ (drone) -> Bool in
                /*
                 Only include drones that have maximum capacity available to them
                 as part of our selection criteria to make tracking easier.
                 */
                drone.availableCommandCapacity() == Constants.DroneCommandCapacity
            })

            let capacity = availableDrones.reduce(0, combine: { (capacity, drone) -> UInt in
                capacity + drone.availableCommandCapacity()
            })

            Log.debug("Capacity \(capacity) Commands \(droneCommandsQueue.count)")

            for drone in availableDrones {
                let takeUpTo = drone.availableCommandCapacity()

                //Find the partition point of the command queue to split it later
                let partitionPoint = Int(takeUpTo) < droneCommandsQueue.count ? Int(takeUpTo - 1) : droneCommandsQueue.count

                //Take the next N commands off of the queue
                let commands = Array(droneCommandsQueue.prefixUpTo(partitionPoint))

                if commands.count > 0 {
                    do {
                        try drone.enqueue(commands)

                        // If enqueing was successful, set the queue equal to the partition point to the end of the queue.
                        droneCommandsQueue = Array(droneCommandsQueue.suffixFrom(partitionPoint))
                    }
                    catch (let error) {
                        Log.error("[\(drone.identifier)] Unable to enqueue commands \(error)")
                    }
                }
            }

            solveIfPossible(droneCommandsQueue.count, drones: drones)
        }
    }

    //MARK: - DroneResponseReceiver Conformance
    func did(error error: String) {
        Log.error("Error when exploring: \(error)")
    }
    
    func did(findRooms rooms: [String]) {
        var commands: [DroneCommand] = []

        for room in rooms {
            /*
             Rooms seem to be unique amongst the results returned.
             Once we've enqueued the commands to both explore and read a room
             we can cache that we've explored it as these are the exhaustive options.
            */
            if !roomHasBeenExplored(room) {
                commands.append(DroneCommand(type: .Explore, roomIdentifier: room))
                commands.append(DroneCommand(type: .Read, roomIdentifier: room))
                exploredRoomCache[room] = true
            }
            else {
                Log.debug("Room \(room) has already been cached and is waiting for processing")
            }
        }

        droneCommandsQueue.appendContentsOf(commands)

        crawl()
    }

    func did(findWriting writing: (text: String, order: Int)) {
        if writing.order >= 0 {
            Log.info("Found some piece of writing from a room! \(writing)")
            writings.append(writing)
        }
    }

    private func roomHasBeenExplored(roomId: String) -> Bool {
        if let explored = exploredRoomCache[roomId] {
            return explored
        }
        return false
    }

    private func solveIfPossible(outstandingCommands: Int, drones: [Drone]) {
        let runningDroneCommands = drones.reduce(0) { (commands, drone) -> UInt in
            commands + drone.activeCommands
        }

        if outstandingCommands == 0
            && runningDroneCommands == 0 {
            solve(writings)
        }
    }

    private func solve(writings: [(String, Int)]) {
        do {
            try solver.solve(writings)
        }
        catch (let error) {
            Log.error("Unable to begin solving, \(error)")
        }
    }
}