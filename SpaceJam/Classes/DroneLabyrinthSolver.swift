//
//  DroneLabyrinthSolver.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/26/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation

/**
 The possible status' of the labyrinth solver.

 - Unknown: The solver is currently in an unknown state.
 - Solving: The solver is actively attempting to solve the labyrinth.
 */
enum LabyrinthSolverStatus {
    case Unknown
    case Solving
}

/**
 The possible status' returned when submitting the labyrinth solution.

 - Solved: The submitted solution correctly solved the labyrinth.
 - Failed: The submitted solution failed to correctly solve the labyrinth.
 */
enum LabyrinthSolutionStatus {
    case Solved
    case Failed
}

/**
 Errors that can be thrown by the labyrinth solver.

 - AlreadySolving: The labyrinth solver is currently already attempting to solve
 the labyrinth.
 */
enum LabyrinthSolverError: ErrorType {
    case AlreadySolving
}

/// A class used to easily encapsulate the writing submission and resolution logic.
final class LabyrinthSolver {
    /// Typealias to wrap up the solution status, and the submitted solution.
    typealias LabyrinthSolverCallback = (LabyrinthSolutionStatus, String) -> Void

    private let API: AirtimeChallengeAPI
    private let callback: LabyrinthSolverCallback
    private var status: LabyrinthSolverStatus = .Unknown

    /**
     Create a new LabyrinthSolver that can be used to submit found writings
     to the report endpoint.

     - parameter API:      The API instance to use when submitting writings.
     - parameter callback: A callback to use when the submission has completed.

     - returns: An intialized solver object that can accept writings and attempt
     to be solved.
     */
    init(API: AirtimeChallengeAPI, callback: LabyrinthSolverCallback) {
        self.API = API
        self.callback = callback
    }

    /**
     Attempt to solve the labyrinth with the provided writings.

     - parameter writings: The writings to use when attempting to solve
     the labyrinth.

     - throws: An `LabyrinthSolverError` describing the issue when trying to
     submit the writings.
     */
    func solve(writings: [(String, Int)]) throws {
        switch status {
        case .Solving:
            throw LabyrinthSolverError.AlreadySolving
        default:
            let concatenated = writings.sort { (first, second) -> Bool in
                first.1 < second.1
                }.reduce("") { (accumulator, writing) -> String in
                    accumulator + writing.0
            }

            status = .Solving

            Log.info("Attempting to solve labyrinth with \(concatenated)")

            API.report(concatenated) { [weak self] (response) in
                self?.status = .Unknown

                switch response.result {
                case .Failure(let error):
                    Log.info("Failed to send the right thing for reporting \(error)")
                    self?.callback(.Failed, concatenated)
                case .Success(let value):
                    Log.info("Succeeded to send value \(value)")
                    self?.callback(.Solved, concatenated)
                }
            }
        }
    }
}
