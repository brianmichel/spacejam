//
//  DroneResponseReceiver.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/26/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation

/**
 An interface describing the various callbacks that we can issue
 after parsing the response from `/drone/%id/commands`
 */
protocol DroneResponseReceiver {
    /**
     The drone did find rooms connected to the requested room.

     - parameter rooms: An array of room ids that are connected.
     */
    func did(findRooms rooms: [String])

    /**
     The drone did find a writing and order pair after reading the room.

     - parameter writing: A tuple describing the text and order discovered
     after reading the room.
     */
    func did(findWriting writing: (text: String, order: Int))

    /**
     An error was encountered from exploring the room, reading the room, 
     or requesting the drone from the API.

     - parameter error: A string describing the error that happened.
     */
    func did(error error: String)
}