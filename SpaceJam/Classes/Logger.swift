//
//  Logger.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/26/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation
import SwiftyBeaver

/// A class to ease the creation of useful log messages.
class Log {
    private static let logger: SwiftyBeaver.Type = {
        let logger = SwiftyBeaver.self
        let console = ConsoleDestination()
        let file = FileDestination()

        logger.addDestination(console)
        logger.addDestination(file)

        return logger
    }()

    class func info(@autoclosure message: () -> Any, _
        path: String = #file, _ function: String = #function, line: Int = #line) {
        logger.info(message, path, function, line: line)
    }

    class func debug(@autoclosure message: () -> Any, _
        path: String = #file, _ function: String = #function, line: Int = #line) {
        logger.debug(message, path, function, line: line)
    }

    class func warn(@autoclosure message: () -> Any, _
        path: String = #file, _ function: String = #function, line: Int = #line) {
        logger.warning(message, path, function, line: line)
    }

    class func error(@autoclosure message: () -> Any, _
        path: String = #file, _ function: String = #function, line: Int = #line) {
        logger.error(message, path, function, line: line)
    }
    
}