//
//  Drone.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/24/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation
import Alamofire

/**
 Potential errors that can be caused when usin the drone.

 - TooManyCommandsToEnqueue:   The number of commands passed to the drone were beyond the
 maximum configured value of commands.
 - NotEnoughCommandQueueSpace: There was not enough space in the drone to enqueue the number
 of commands passed in.
 - NoCommandsGiven:            There were no commands passed in to the drone.
 */
enum DroneErrorType: ErrorType {
    case TooManyCommandsToEnqueue
    case NotEnoughCommandQueueSpace
    case NoCommandsGiven
}

/// A model representation of the Drone object returned from the API.
final class Drone {
    let identifier: String
    let responseReceiver: DroneResponseReceiver
    let API: AirtimeChallengeAPI
    let maximumCommands: UInt

    private(set) var activeCommands: UInt = 0

    /**
     Initialize a new drone object that contains the desired values.

     - parameter identifier:       The identifier of the drone used for API calls.
     - parameter maximumCommands:  The maximum number of commands that this drone can accept.
     - parameter responseReceiver: The object which will receive response information after it has been parsed.
     - parameter droneAPI:         The API instance to use for subsequent network calls.

     - returns: <#return value description#>
     */
    init(identifier: String,
         maximumCommands: UInt,
         responseReceiver: DroneResponseReceiver,
         droneAPI: AirtimeChallengeAPI) {
        self.identifier = identifier
        self.responseReceiver = responseReceiver
        self.API = droneAPI
        self.maximumCommands = maximumCommands
    }

    /**
     Attempt to enqueue a list of commands for the drone.

     - parameter commands: The list of commands to attempt to enqueue.

     - throws: One of the values listed in `DroneErrorType` is conditions are met.
     */
    func enqueue(commands: [DroneCommand]) throws {
        let commandCountToEnqueue = UInt(commands.count)

        if commandCountToEnqueue > maximumCommands {
            throw DroneErrorType.TooManyCommandsToEnqueue
        }
        else if commandCountToEnqueue > availableCommandCapacity() {
            throw DroneErrorType.NotEnoughCommandQueueSpace
        }
        else if commandCountToEnqueue == 0 {
            throw DroneErrorType.NoCommandsGiven
        }

        Log.info("Drone \(identifier) enqueueing \(commandCountToEnqueue) with current count \(activeCommands)")

        activeCommands += UInt(commandCountToEnqueue)

        API.order(drone: identifier, commands: commands) { [weak self] (response) in
            self?.processResponse(response)
        }
    }

    /**
     The number of available command slots currently available
     on the drone.

     - returns: The number of command slots available.
     */
    func availableCommandCapacity() -> UInt {
        return maximumCommands - activeCommands
    }
}

extension Drone {
    private struct ResponseKeys {
        // For some reason the documentation calls this "connectedRooms"
        static let ConnectedRooms = "connections"
        static let Writing = "writing"
        static let Order = "order"
        static let Error = "error"
    }
    
    private func processResponse(response: Response<AnyObject, NSError>) {
        guard let urlResponse = response.response else {
            return
        }

        activeCommands = 0

        switch urlResponse.statusCode {
        case 200:
            // Continue Processing
            switch response.result {
            case .Failure(let error):
                responseReceiver.did(error: "Error deserializing response! \(error)")
            case .Success(let data):
                guard let dictionary = data as? [String: [String: AnyObject]] else {
                    responseReceiver.did(error: "Data format was not strings as keys, with nested string to string mapping")
                    return
                }

                for (_, value) in dictionary {
                    if value.keys.contains(ResponseKeys.ConnectedRooms) {
                        if let rooms = value[ResponseKeys.ConnectedRooms] as? [String] {
                            responseReceiver.did(findRooms: rooms)
                        }
                    }
                    else if value.keys.contains(ResponseKeys.Writing) {
                        if let
                            writing = value[ResponseKeys.Writing] as? String,
                            order = value[ResponseKeys.Order] as? Int {
                            responseReceiver.did(findWriting: (writing, order))
                        }
                    }
                    else if value.keys.contains(ResponseKeys.Error) {
                        if let error = value[ResponseKeys.Error] as? String {
                            responseReceiver.did(error: error)
                        }
                    }
                    else {
                        // There is nothing supported that we can parse.
                    }
                }
            }
        case 409:
            fallthrough
        case 400:
            // Drone is busy, sorry!
            responseReceiver.did(error: "Drone \(identifier) is busy! (\(activeCommands))")
        case 404:
            // Drone doesn't exist, huh?
            responseReceiver.did(error: "That drone (\(identifier)) didn't exist!")
        default:
            // Some other thing happened and we have no idea.
            responseReceiver.did(error: "Something bad happened with the server :(")
        }
    }
}
