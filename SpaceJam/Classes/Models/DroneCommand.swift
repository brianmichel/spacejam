//
//  DroneCommand.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/24/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import Foundation

/**
 The types of Drone commands that are possible.

 - Explore: Explore the room to unveil other connected rooms
 if possible.
 - Read:    Read the room to attempt to discover writings and
 orders
 */
enum DroneCommandType: String {
    case Explore = "explore"
    case Read = "read"
}

/**
 A structure documenting a command that we can issue to the API on
 behalf of the drone commander for a specific room.
 */
struct DroneCommand {
    /// The type of command to issue.
    let type: DroneCommandType
    /// The identifier of the room to issue the command to.
    let roomIdentifier: String
    /// An identifier of the command that is issued to the API.
    let commandId: UInt32 = arc4random()

    /**
     Serialize the `DroneCommand` into an easily serializable structure
     that will eventually be turned into JSON.

     - returns: A tuple with a string and a map of string to string that mirrors
     the structure of commands that get sent to the API.
     */
    func serialize() -> (String, [String: String]) {
        return (
            String(commandId),
            [
                type.rawValue: roomIdentifier
            ]
        )
    }
}

