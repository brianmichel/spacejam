//
//  ViewController.swift
//  SpaceJam
//
//  Created by Brian Michel on 7/24/16.
//  Copyright © 2016 Brian Michel. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let api = AirtimeChallengeAPI(commander: "brian.michel@gmail.com")
    var strategy: DroneCrawlerStrategy?

    override func viewDidLoad() {
        super.viewDidLoad()

        api.start { [weak self] (response) in
            // setup strategy
            guard let strongSelf = self else {
                return
            }

            switch response.result {
            case .Failure(let error):
                Log.error("Error fetching start call! \(error)")
            case .Success(let value):
                if let startResponse = value as? [String: AnyObject] {
                    guard
                        let roomId = startResponse["roomId"] as? String,
                        let droneIds = startResponse["drones"] as? [String] else {
                            return
                    }

                    strongSelf.strategy = DroneCrawlerStrategy(initialRoomId: roomId, droneIds: droneIds, API: strongSelf.api)
                }
            }
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

